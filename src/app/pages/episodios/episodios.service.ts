import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { episodiosI } from 'src/app/models/episodes.interface';

@Injectable({
  providedIn: 'root'
})
export class EpisodiosService {

  episodiosListado : any;

  constructor(private http: HttpClient) { }

  getAllEpisodes(currentPage: number): Observable<episodiosI> {
    return this.http.get<episodiosI>(`https://rickandmortyapi.com/api/episode?page=${currentPage}`)
  }
}


// Si falla algo con el tipado, es porque está en string[] y no en any como los demás.