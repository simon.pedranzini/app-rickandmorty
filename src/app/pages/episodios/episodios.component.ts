import { episodiosI } from './../../models/episodes.interface';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EpisodiosService } from './episodios.service';

@Component({
  selector: 'app-episodios',
  templateUrl: './episodios.component.html',
  styleUrls: ['./episodios.component.scss']
})
export class EpisodiosComponent implements OnInit {

  episodios: any;
  currentPage = 1;
  maxPages = 3;

  private episodesSubsription$?: Subscription;

  constructor(private episodioSrv: EpisodiosService) { }

  ngOnInit(): void {
    this.getEpisode();
    // console.log('ESTO =>', this.getEpisode(1))
      }

  getEpisode() {
    this.episodesSubsription$ = this.episodioSrv.getAllEpisodes(this.currentPage)
    .subscribe((datos) => (
    this.episodios = datos.results))
    // this.episodioSrv.getAllEpisodes(this.currentPage).subscribe((data) => console.log(data.results))

  }

  ngOnDestroy(): void {
    this.episodesSubsription$?.unsubscribe();
  }

  prevPage() {
    if (this.currentPage > 1) {
      this.currentPage -= 1;
      this.getEpisode()
    } else {
      alert('Estás en la primera página')
    }  }

  nextPage() {
    if (this.currentPage < this.maxPages) {
      this.currentPage += 1;
      this.getEpisode()
    } else {
      alert('Máximo de páginas')
    }
  }
}
