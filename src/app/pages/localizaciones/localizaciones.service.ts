import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NumberValueAccessor } from '@angular/forms';
import { Observable } from 'rxjs';
import { localizacionesI } from 'src/app/models/localizaciones.interface';

@Injectable({
  providedIn: 'root'
})
export class LocalizacionesService {

  // private currentPage : number = 5;

  // private urlApi = `https://rickandmortyapi.com/api/location?page=${this.currentPage}`

  constructor( private http: HttpClient) { }

  getAllLocalizaciones(currentPage: number) : Observable<localizacionesI> {
    return this.http.get<localizacionesI>(`https://rickandmortyapi.com/api/location?page=${currentPage}`);
  }

  // getAllLocalizaciones() : Observable<localizaciones> {
  //   return this.http.get<localizaciones>(this.urlApi);
  // }
  // NO FUNCIONA, SI EL PROCESO DE SUMAR UNO DEL COMPONENT.TS PERO NO AQUÍ, NUNCA TERMINA DE PASARLE EL NUMERO


}
