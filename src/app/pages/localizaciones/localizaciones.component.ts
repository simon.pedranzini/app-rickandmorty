import { LocalizacionesService } from './localizaciones.service';
import { Component, OnInit } from '@angular/core';
import { localizacionesI } from 'src/app/models/localizaciones.interface';

@Component({
  selector: 'app-localizaciones',
  templateUrl: './localizaciones.component.html',
  styleUrls: ['./localizaciones.component.scss']
})
export class LocalizacionesComponent implements OnInit {

  localizacionesListado: any;
  page = 1;
  totalPages: any;

  constructor(private localizacionesSrv: LocalizacionesService) { }

  ngOnInit(): void {
    this.getAllLocalizaciones();
  }

  getAllLocalizaciones() {
    this.localizacionesSrv.getAllLocalizaciones(this.page)
      .subscribe((data) =>
      (this.localizacionesListado = (data.results),
        // console.log(data),
        this.totalPages = data.info.pages //CUIDADO CON EL TIPADO DE LA INTERFACE!!!!
      ))
  };

  nextPage() {
    // console.log(this.page)
    if (this.page < this.totalPages) {
      this.page = this.page + 1;
      this.getAllLocalizaciones();
    } else {
      alert('No hay mas páginas')
    }
  }

  prevPage() {
    // console.log(this.page)
    if (this.page > 1) {
      this.page = this.page - 1;
      this.getAllLocalizaciones();
    } else {
      alert('Estás en la primera página')
    }
  }

  ngOnDestroy(): void {
    this.localizacionesSrv.getAllLocalizaciones(this.page); //ESTO NO HACE NADA, PARA DESUSCRIBIRSE HAY QUE METER EL OBSERVABLE EN UNA FUNCION Y DESUSCRIBIRLA
  }
}
