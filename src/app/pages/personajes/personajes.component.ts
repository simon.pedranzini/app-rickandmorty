import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PersonajesService } from './personajes.service';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.scss']
})
export class PersonajesComponent implements OnInit {

  personajesLista: any;
  totalPages: any ;
  currentPage = 1;
  private personajesSubscription$?: Subscription ;

  constructor(private personajesSrv: PersonajesService) { }

  ngOnInit(): void {
    this.todosPersonajes();
    console.log(this.currentPage)
  }

todosPersonajes() {
  this.personajesSubscription$ = this.personajesSrv.getAllCharacters(this.currentPage)
  .subscribe((datitos) => (
    this.personajesLista = datitos.results,
    this.totalPages = datitos.info.pages)
  );
}

  // todosPersonajes() {
  //   this.personajesSrv.getAllCharacters(this.currentPage)
  //   .subscribe((datitos) => (
  //   this.personajesLista = datitos.results,
  //   this.totalPages = datitos.info.pages
  //   ));
  // }

  prevPage() {
    if (this.currentPage > 1) {
      this.currentPage -= 1;
      this.todosPersonajes();
    } else {
      alert('Estás en la primera página')
    }
  }

  nextPage() {
    if (this.currentPage < this.totalPages) {
      this.currentPage += 1;
      this.todosPersonajes()
    } else {
      alert('Llegaste a la ultima pagina')
    }
  }

  ngOnDestroy() {
    this.personajesSubscription$?.unsubscribe;
  }

}
