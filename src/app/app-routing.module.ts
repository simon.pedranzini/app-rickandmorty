import { LocalizacionesComponent } from './pages/localizaciones/localizaciones.component';
import { EpisodiosComponent } from './pages/episodios/episodios.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonajesComponent } from './pages/personajes/personajes.component';

const routes: Routes = [
  {
    path: 'inicio', component: HomeComponent
  },
  {
    path: 'personajes', component: PersonajesComponent
  },
  {
    path: 'episodios', component: EpisodiosComponent
  },
  {
    path: 'localizaciones', component: LocalizacionesComponent
  },
  {
    path: '**', redirectTo: 'inicio', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
